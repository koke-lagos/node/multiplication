# NodeJS y TypeScript

### ⚙️ Installs
  * Instalas dependencias del proyecto `npm install`

### 🔧 Executions
  * Ejecutar `npm start`
  * Ejecutar en modo desarrollo `npm run dev` o `npm run dev:nodemon`

### 🔧 Dependencies
  * Instalar `npm install yargs`

### 🔧 Dev Dependencies
  * Instalar `npm install --save-dev @types/yargs`

### ✅ Node con TypeScript
  * Instalar TypeScript y demás dependencias
    ```
    npm i -D typescript @types/node ts-node nodemon rimraf
    ```

  * Inicializar el archivo de configuración de TypeScript ( Se puede configurar al gusto)
    ```
    npx tsc --init --outDir dist/ --rootDir src
    ```

  * Crear archivo de configuración Nodemon - nodemon.json
    ```
    {
      "watch": ["src"],
      "ext": ".ts,.js",
      "ignore": [],
      "exec": "npx ts-node ./src/app.ts"
    }
    ```

  * Crear scripts para dev, build y start en package.json
    ```
    "dev": "nodemon",
    "build": "rimraf ./dist && tsc",
    "start": "npm run build && node dist/app.js"  
    ```

### ✅ Configuración Jest con TypeScript
  * Instalaciones de desarrollo (super test es útil para probar Express)
    ```
    npm install -D jest @types/jest ts-jest supertest
    ```
  
  * Crear archivo de configuración de Jest
    ```
    npx jest --init

    ✅ Would you like to use Jest when running "test" script in "package.json"? » yes
    ✅ Would you like to use Typescript for the configuration file? » yes
    ✅ Choose the test environment that will be used for testing » node
    ✅ Do you want Jest to add coverage reports? » no
    ✅ Which provider should be used to instrument code for coverage? » v8
    ✅ Automatically clear mock calls, instances, contexts and results before every test? » no
    ```

  * En el archivo jest.config.js configurar
    ```
    preset: 'ts-jest',
    testEnvironment: 'jest-environment-node',
    ```
  * Crear scripts en el package.json
    ```
    "test": "jest",
    "test:watch": "jest --watch",
    "test:coverage": "jest --coverage",
    ```

### 📌 Author
  * Jorge Lagos Álvarez