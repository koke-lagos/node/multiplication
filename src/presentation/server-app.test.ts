import { CreateTable } from '../domain/use-cases/create-table.use-case'
import { SaveFile } from '../domain/use-cases/save-file.use-case'
import { ServerApp, RunOptions } from './server-app'

describe('server-app', () => {

    beforeEach(() => {
        jest.clearAllMocks()
    })

    const runOptions: RunOptions = {
        base: 2,
        limit: 10,
        showTable: false,
        fileName: 'test-name',
        fileDestination: 'test-destination',
    }

    test('should create server-app instance', () => {
        const serverApp = new ServerApp()

        expect(serverApp).toBeInstanceOf(ServerApp)
        expect(typeof ServerApp.run).toBe('function')
    })

    test('should run server-app with options', () => {
        const logSpy = jest.spyOn(console, 'log')
        const createTableSpy = jest.spyOn(CreateTable.prototype, 'execute')
        const saveFileSpy = jest.spyOn(SaveFile.prototype, 'execute')

        ServerApp.run(runOptions)

        expect(logSpy).toHaveBeenCalledTimes(2)
        expect(logSpy).toHaveBeenCalledWith('Server running...')
        expect(logSpy).toHaveBeenLastCalledWith('File created...')

        expect(createTableSpy).toHaveBeenCalledTimes(1)
        expect(createTableSpy).toHaveBeenCalledWith({ 
            base: runOptions.base,
            limit: runOptions.limit,
        })

        expect(saveFileSpy).toHaveBeenCalledTimes(1)
        expect(saveFileSpy).toHaveBeenCalledWith({
            fileContent: expect.any(String),
            fileDestination: runOptions.fileDestination,
            fileName: runOptions.fileName,
        })
    })

    test('should run server-app with custom values mocked', () => {
        const logMock = jest.fn()
        const logErrorMock = jest.fn()
        const createTableMock = jest.fn().mockReturnValue('1 x 2 = 2')
        const saveFileMock = jest.fn().mockReturnValue(true)

        console.log = logMock
        console.error = logErrorMock
        CreateTable.prototype.execute = createTableMock
        SaveFile.prototype.execute = saveFileMock

        ServerApp.run(runOptions)

        expect(logMock).toHaveBeenCalledWith('Server running...')
        expect(createTableMock).toHaveBeenCalledWith({
            base: runOptions.base,
            limit: runOptions.limit,
        })
        expect(saveFileMock).toHaveBeenCalledWith({
            fileContent: '1 x 2 = 2',
            fileDestination: runOptions.fileDestination,
            fileName: runOptions.fileName,
        })
        expect(logMock).toHaveBeenCalledWith('File created...')
        // expect(logErrorMock).not.toHaveBeenCalledWith()
    })

})