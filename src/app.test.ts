import { ServerApp } from './presentation/server-app'

describe('app', () => {

    test('should call server.run with values', async () => {
        const serverRunMock = jest.fn()

        ServerApp.run = serverRunMock

        process.argv = ['node', 'app.ts','--base=10', '--limit=5', '--show', '--name=test-file', '--destination=test-destination']
    
        await import('./app')
    
        expect(serverRunMock).toHaveBeenCalledWith({ 
            base: 10, 
            limit: 5, 
            showTable: true, 
            fileName: 'test-file', 
            fileDestination: 'test-destination' 
        })
    })

})