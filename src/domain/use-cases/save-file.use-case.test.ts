import fs from 'fs'

import { Options, SaveFile } from './save-file.use-case'

describe('save-file.use-case', () => {

    const options: Options = {
        fileContent: 'test content',
        fileDestination: 'outputs',
        fileName: 'table',
    }

    const customOptions: Options = {
        fileContent: 'custom content',
        fileDestination: 'custom-outputs',
        fileName: 'custom-table-name',
    }

    const filePath = `${options.fileDestination}/${options.fileName}.txt`
    const customFilePath = `${customOptions.fileDestination}/${customOptions.fileName}.txt`

    afterEach(() => {
        const outputFolderExists = fs.existsSync(options.fileDestination!)
        if (outputFolderExists) fs.rmSync(options.fileDestination!, { recursive: true })

        const customOutputFolderExists = fs.existsSync(customOptions.fileDestination!)
        if (customOutputFolderExists) fs.rmSync(customOptions.fileDestination!, { recursive: true })
    })

    test('should save file with default values', () => {
        const saveFile = new SaveFile()
        const result = saveFile.execute(options)
        const fileExists = fs.existsSync(filePath)
        const fileContent = fs.readFileSync(filePath, { encoding: 'utf-8' })

        expect(result).toBe(true)
        expect(fileExists).toBe(true)
        expect(fileContent).toBe(options.fileContent)
    })

    test('should save file with custom values', () => {
        const saveFile = new SaveFile()
        const result = saveFile.execute(customOptions)
        const fileExists = fs.existsSync(customFilePath)
        const fileContent = fs.readFileSync(customFilePath, { encoding: 'utf-8' })

        expect(result).toBe(true)
        expect(fileExists).toBe(true)
        expect(fileContent).toBe(customOptions.fileContent)
    })

    test('should return false if directory could not be created', () => {
        const saveFile = new SaveFile()
        const mkdirSpy = jest.spyOn(fs, 'mkdirSync').mockImplementation(() => {
            throw new Error('error')
        })

        const result = saveFile.execute(customOptions)

        expect(result).toBe(false)

        mkdirSpy.mockRestore()
    })

    test('should return false if file could not be created', () => {
        const saveFile = new SaveFile()
        const writeFileSpy = jest.spyOn(fs, 'writeFileSync').mockImplementation(() => {
            throw new Error('this is a custom writing error message')
        })

        const result = saveFile.execute({ fileContent: 'hola' })

        expect(result).toBe(false)

        writeFileSpy.mockRestore()
    })

})